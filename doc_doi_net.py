from typing import Counter
import sqlite_utils
import csv

import networkx

db = sqlite_utils.Database("data/risis_doc_webe.db")

with open("data/sel_on_overton_meta_docs_20210710.csv", "r") as f:
    selection = set(doc['pdf_document_id'] for doc in csv.DictReader(f))

    pdf_doi_graph = networkx.DiGraph()
    # add document nodes
    pdfs = [(d['pdf_document_id'], dict((k, v) for (k, v) in d.items()
                                        if v is not None)) for d in list(db['pdf'].rows)]

    pdf_doi_graph.add_nodes_from(
        pdfs)

    generate_doc_links = """SELECT  c.pdf_document_id as source, doi as target, count(*) as weight
    FROM pdf_doi_citation as c 
    GROUP BY source, target;
    """

    edges = ((link['source'], link['target'], {"weight": link["weight"]}) for link in db.query(
        generate_doc_links) if link['source'] in selection)

    pdf_doi_graph.add_edges_from(edges)
    pdf_doi_graph.remove_nodes_from(
        (n for n in list(pdf_doi_graph.nodes) if pdf_doi_graph.degree[n] < 2))

    networkx.readwrite.gexf.write_gexf(
        pdf_doi_graph, 'data/overton_pdf_doi.gexf')
