from genericpath import exists
import os
from typing import Counter
import config
import ndjson
import json
from ural.lru import LRUTrie
import csv
import networkx


def load_hyphe_corpus():
    with open(config.HYPHE_CORPUS_PATH, 'r') as f:
        return json.load(f)


def create_lru_trie_from_hyphe_corpus(hyphe_corpus):
    trie = LRUTrie()
    for we in hyphe_corpus["webentities"]:
        for prefix in we["PREFIXES AS LRU"]:
            trie.set_lru(prefix, we)
    return trie


def add_doc_edges_to_we_network(we_edges, webEntity_network, web_entities, ignore_selfloops=False):
    for edge, nb_doc in we_edges.items():
        # ignore selfloop
        if not ignore_selfloops or edge[0] != edge[1]:
            for n in edge:
                if not n in webEntity_network:
                    webEntity_network.add_node(n)
                    networkx.set_node_attributes(webEntity_network, {n: dict(
                        [("layer", "overton")]+[(k, v) for k, v in web_entities[n].items() if k in ["NAME", "ID"]])})
                elif webEntity_network.nodes[n]["layer"] == "hyphe":
                    # add overton in layer flag
                    networkx.set_node_attributes(
                        webEntity_network, {n: {"layer": "hyphe_overton"}})
            if edge[0] in webEntity_network and edge[1] in webEntity_network[edge[0]]:
                networkx.set_edge_attributes(
                    webEntity_network, {edge: {"nb_doc_citation": nb_doc, "layer": "hyphe_overton"}})
            else:
                networkx.add_path(webEntity_network, edge,
                                  nb_doc_citation=nb_doc, layer="overton")


def load_overton_metadata(filter=None):
    if not os.path.exists(config.OVERTON_METADATA_PATH):
        raise(Exception(f"{config.OVERTON_METADATA_PATH} does not exist"))
    # Prepare Hypeh data
    hyphe_corpus = load_hyphe_corpus()
    lru_trie = create_lru_trie_from_hyphe_corpus(hyphe_corpus)
    web_entities = {we['ID']: we for we in hyphe_corpus["webentities"]}
    # data structures
    nb_unrecognized = 0
    nb_doc = 0
    documents = []
    policyDocumentId_to_we = {}

    for dirpath, directories, files in os.walk(config.OVERTON_METADATA_PATH):
        print(dirpath)
        print(directories)
        print(f"Processing {len(files)} files")
        for file in files:
            if os.path.splitext(file)[1] == ".json":
                print(file)
                with open(os.path.join(dirpath, file), "r") as f:
                    docs = ndjson.load(f)
                    for doc in docs:
                        nb_doc += 1
                        # PDF level WE
                        pdf_we = lru_trie.match(doc['pdf_url'])
                        # Document level WE
                        doc_we = lru_trie.match(doc['policy_document_url'])
                        # Source level WE
                        source_we = lru_trie.match(doc['policy_source_url'])

                        doc['pdf_we_id'] = pdf_we['ID'] if pdf_we else None
                        doc['doc_we_id'] = doc_we['ID'] if doc_we else None
                        doc['source_we_id'] = source_we['ID'] if source_we else None
                        if doc_we:
                            if not "nb_overton_docs" in web_entities[doc_we['ID']]:
                                web_entities[doc_we['ID']
                                             ]["nb_overton_docs"] = 0
                            web_entities[doc_we['ID']]["nb_overton_docs"] += 1
                            policyDocumentId_to_we[doc['policy_document_id']
                                                   ] = doc_we['ID']
                        else:
                            nb_unrecognized += 1
                        documents.append(doc)
        print(
            f"could match {nb_doc-nb_unrecognized}/{nb_doc} docs ({(nb_doc-nb_unrecognized)/nb_doc*100:.2f}%)")
        # report on nb docs by web
        nb_docs_by_webentities_report = "data/nb_docs_by_webentitites.csv"
        with open(nb_docs_by_webentities_report, "w") as we_report_f:
            fields_to_export = ["ID", "NAME", "HOME_PAGE", "nb_overton_docs"]
            report = csv.DictWriter(
                we_report_f, fields_to_export)
            report.writeheader()
            nb_we_with_docs = 0
            for we in list(web_entities.values()):
                if "nb_overton_docs" in we:
                    nb_we_with_docs += 1
                report.writerow(dict((k, v) for k, v in we.items(
                ) if k in fields_to_export))
            print(f"{nb_we_with_docs} web entities with at least one doc")
            print(
                f"nb docs by web entities printed in {nb_docs_by_webentities_report}")

        # network webE - doc - webE

        we_edges = Counter()
        for doc in documents:
            if 'doc_we_id' in doc and doc['doc_we_id']:
                for policy_doc_id in doc["policy_document_ids_cited"]:
                    try:
                        sourceWE = doc['doc_we_id']
                        targetWE = policyDocumentId_to_we[policy_doc_id]
                        we_edges[(sourceWE, targetWE)] += 1
                    except Exception as e:
                        pass
        print(
            f"generated {len(we_edges.keys())} WE -doc-> WE edges ({len([s for (s,t) in we_edges.keys() if s==t])} self loops)")
        if config.HYPHE_NETWORK_PATH and os.path.exists(config.HYPHE_NETWORK_PATH):
            # load hyphe network
            webEntity_network = networkx.readwrite.gexf.read_gexf(
                config.HYPHE_NETWORK_PATH, node_type=int)
            # flag hyphe layer
            networkx.set_edge_attributes(
                webEntity_network, "hyphe", name="layer")
            networkx.set_node_attributes(
                webEntity_network, "hyphe", name="layer")

            add_doc_edges_to_we_network(
                we_edges, webEntity_network, web_entities)

            networkx.readwrite.gexf.write_gexf(
                webEntity_network, 'data/mixed_overton_hyphe.gexf')
            print("Wrote mixed_overton_hyphe.gexf network")

        webEntity_linked_by_overton_network = networkx.DiGraph()
        add_doc_edges_to_we_network(
            we_edges, webEntity_linked_by_overton_network, web_entities)

        networkx.readwrite.gexf.write_gexf(
            webEntity_linked_by_overton_network, 'data/overton_hyphe.gexf')
        print("Wrote overton_hyphe.gexf network")

        return documents, policyDocumentId_to_we, web_entities


def overton_doc_to_sqlite(documents, web_entities):
    from sqlite_utils import Database
    db = Database("data/risis_doc_webe.db", recreate=True)
    source_fields = ["policy_source_id", "policy_source_title",
                     "policy_source_url", "source_we_id"]
    source_extra_meta = ["policy_source_type", "source_tags",
                         "policy_source_country", "policy_source_region"]
    doc_fields = ["policy_document_id", "policy_source_id", "title", "translated_title",
                  "policy_document_url", "snippet", "published_on", "policy_source information", "policy_document_series", "overton_policy_document_series",  "doc_we_id"]
    doc_extra_meta = ["authors", ]
    pdf_fields = ["pdf_document_id", "policy_document_id", "policy_source_id", "pdf_title", "pdf_url", "overton_document_url",
                  "language", "pdf_thumbnail", "fulltext_file", "pdf_we_id"]
    pdf_extra_meta = ["sdgcategories", "topics",
                      "classifications", "entities"]
    link_pdf_doc = ["pdf_document_id", "policy_document_id"]
    link_pdf_doi = ["pdf_document_id", "doi"]
    # prepare data

    def cast_doc(doc, fields):
        d = dict((k, v) for k, v in doc.items() if k in fields)
        for f in fields:
            if f not in d:
                d[f] = None
        return d
    sources = {}
    sources_annexs = {f: [] for f in source_extra_meta}
    sqlDocs = {}
    sqlDocs_annexs = {f: [] for f in doc_extra_meta}
    pdf = []
    pdf_annnexs = {f: [] for f in pdf_extra_meta}
    pdf_doc_citations = []
    pdf_doi_citations = []

    def update_annexs(annexs, doc, key_field):
        for k in annexs:
            annexs[k] += [{key_field: doc[key_field], k:v} for v in doc[k]]

    for doc in documents:
        if doc["policy_source_id"] not in sources:
            sources[doc["policy_source_id"]] = cast_doc(doc, source_fields)
            update_annexs(sources_annexs, doc, "policy_source_id")
        if doc["policy_source_id"] not in sqlDocs:
            sqlDocs[doc["policy_document_id"]] = cast_doc(doc, doc_fields)
            update_annexs(sqlDocs_annexs, doc, "policy_document_id")
        pdf.append(cast_doc(doc, pdf_fields))
        update_annexs(pdf_annnexs, doc, "pdf_document_id")
        pdf_doc_citations += [{"pdf_document_id": doc["pdf_document_id"],
                               "policy_document_id":v} for v in doc["policy_document_ids_cited"]]
        pdf_doi_citations += [{"pdf_document_id": doc["pdf_document_id"],
                               "doi":v} for v in doc["dois_cited"]]
    del(documents)
    print("data ready for insert")
    db["web_entities"].insert_all(web_entities, pk="ID")
    del(web_entities)
    print("web entities inserted")

    db["policy_source"].insert_all(sources.values(
    ), pk="policy_source_id", foreign_keys=[("source_we_id", "web_entities", "ID")])
    del(sources)
    print("sources inserted")
    for f in source_extra_meta:
        db[f].insert_all(sources_annexs[f], foreign_keys=[
                         ("policy_source_id", "policy_source", "policy_source_id")])
    del(sources_annexs)
    print("sources annexs inserted")
    db["document"].insert_all(sqlDocs.values(), pk="policy_document_id", foreign_keys=[
        ("doc_we_id", "web_entities", "ID"), ("policy_source_id", "policy_source", "policy_source_id")])
    del(sqlDocs)
    print("documents inserted")
    for f in sqlDocs_annexs:
        db[f].insert_all(sqlDocs_annexs[f], foreign_keys=[
                         ("policy_document_id", "document", "policy_document_id")], batch_size=10000)
    del(sqlDocs_annexs)
    print("documents annexs inserted")
    db["pdf"].insert_all(pdf, pk="pdf_document_id", column_order=pdf_fields, foreign_keys=[
                         ("pdf_we_id", "web_entities", "ID"), ("policy_document_id", "document", "policy_document_id"), ("policy_source_id", "policy_source", "policy_source_id")],  batch_size=10000)
    del(pdf)
    print("pdf inserted")
    for f in pdf_extra_meta:
        db[f].insert_all(pdf_annnexs[f], foreign_keys=[
                         ("pdf_document_id", "pdf", "pdf_document_id")], batch_size=10000)
    del(pdf_annnexs)
    print("pdf annexs inserted")
    db["pdf_doc_citation"].insert_all(pdf_doc_citations, foreign_keys=[(
        "pdf_document_id", "pdf", "pdf_document_id"), ("policy_document_id", "document", "policy_document_id")], batch_size=10000)
    #
    # policy_document_ids_cited dois_cited
    db["pdf_doi_citation"].insert_all(pdf_doi_citations, foreign_keys=[(
        "pdf_document_id", "pdf", "pdf_document_id")], batch_size=10000)


documents, policyDocumentId_to_we, web_entities = load_overton_metadata()
overton_doc_to_sqlite(documents, web_entities.values())
