from typing import Counter
import sqlite_utils
import csv

import networkx

db = sqlite_utils.Database("data/risis_doc_webe.db")

with open("data/sel_on_overton_meta_docs_20210710.csv", "r") as f:
    selection = set(doc['pdf_document_id'] for doc in csv.DictReader(f))

    doc_doc_graph = networkx.DiGraph()
    # add document nodes
    docs = [(d['policy_document_id'], dict((k, v) for (k, v) in d.items()
                                           if v is not None)) for d in list(db['document'].rows)]

    doc_doc_graph.add_nodes_from(
        docs)

    generate_doc_links = """SELECT  pdf.policy_document_id as source, c.policy_document_id as target, count(*) as weight, GROUP_CONCAT(c.pdf_document_id) as pdf_sources
    FROM pdf_doc_citation as c LEFT JOIN pdf as pdf ON pdf.pdf_document_id= c.pdf_document_id
    GROUP BY source, target;
    """

    edges = ((link['source'], link['target'], {"weight": link["weight"]}) for link in db.query(
        generate_doc_links) if len(set(link['pdf_sources'].split(',')) & selection) > 0)

    doc_doc_graph.add_edges_from(edges)
    doc_doc_graph.remove_nodes_from(
        (n for n in list(doc_doc_graph.nodes) if doc_doc_graph.degree[n] == 0))

    networkx.readwrite.gexf.write_gexf(
        doc_doc_graph, 'data/overton_doc_doc.gexf')
