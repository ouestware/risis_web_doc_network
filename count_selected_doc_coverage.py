from typing import Counter
import sqlite_utils
import csv

db = sqlite_utils.Database("data/risis_doc_webe.db")

with open("data/sel_on_overton_meta_docs_20210710.csv", "r") as f:
    selection = list(csv.DictReader(f))
    nb_doc_found = 0
    nb_doc_with_we = 0
    web_entities = Counter()
    for doc in selection:
        try:
            db_pdf = db["pdf"].get(doc['pdf_document_id'])
            nb_doc_found += 1
            if db_pdf['pdf_we_id']:
                nb_doc_with_we += 1
                web_entities[db_pdf['pdf_we_id']] += 1
            else:
                db_doc = db["document"].get(db_pdf['policy_document_id'])
                if db_doc['doc_we_id']:
                    print("we in doc not in pdf")
                    nb_doc_with_we += 1
                    web_entities[db_doc['doc_we_id']] += 1
        except sqlite_utils.NotFoundError as e:
            pass
    print(f"{nb_doc_found} {nb_doc_found/len(selection)*100}% documents found on {len(selection)}")

    print(f"{nb_doc_with_we} {nb_doc_with_we/len(selection)*100:.2f}% documents has web entity")
    print("\n".join((["web_entity_id,nb_doc"] +
                     [f"{weID},{nb}" for (weID, nb) in web_entities.items()])))
