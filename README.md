# RISIS web doc network

Compute a multilayer network from a Hyphe web corpus and a document citation analysis datasets.

## Prepare Data

You fist need to copy relevant data into the data folder.
As expressed in the [config.py](./config.py) file, two data-sets are required:

- overton metadata NDJSON files to be stored in _./data/overton_metadata_
- Hyphe corpus (Web entity JSON export) to be stored in _./data/_
- Hyphe network (export from hyphe network page) to be stored in _./data/_

The Hyphe network is not mandatory. If present a multilayer network will be generated mixing overton edges into the hyperlink one.

## Prepare Python environment

Requirements:

- python > 3.8
- all deps are listed in requirements.txt

One way to set up the runtime env is to use pyenv and virtualenv as follow:

```bash
pyenv install 3.8.3
pyenv virtualenv 3.8.3 risis
pyenv activate risis
pip install -r requirements.txt
```

## Run the script

```
(risis) user@user:~/somepath$ python match_doc_to_web_entity.py
```

## outputs

- _./data/nb_docs_by_webentities.csv_: list the number of overton docs matched for each hyphe web entity
- _overton_hyphe.gexf_: network of web entities linked by overton document citations
- _mixed_overton_hyphe.gexf_: multilayer network of HTTP and doc citation edges. In this network edges has two important attributes. The layer attribute indicates if the edge is present in hyphe, overton or both layer (hyphe_overton). The nb_doc_citation is the weight to be used for the overton layer.

## multilayer analysis

### divide and conquer

One first simple way is to analyze the layers as separated graphs.
There are two different network files which can be both opened and analyzed with Gephi in parallel.

### using the multilayer network

The miwed network allows to drive the multi layer analysis in one workspace in Gephi.

First one needs to convert tha GEXF into gephi format to allow edge filtering:

- open the mixed_overton_hyphe.gexf file with Gephi
- save the network as a gephi file to allow edge filtering [see Gephi issue](https://github.com/gephi/gephi/issues/2096#issuecomment-892556676)
- open the gephi file

We the gephi file, one way to analysis both layers in gephi is to use filters.

#### colour nodes and edges by layer

By using Gephi color options, color nodes and edges depending on their "layer" attribute. Chose the same color for both node and edge.

#### the hyphe layer

By using Filter / library / attributes / partition / layer (edge), one can decide to keep only the edges that are in the hyphe layer by selecting both "hyphe" and "hyphe_overton" partitions.
Then the layout can be rendered, node size adjusted...
Note that all nodes are part of the hyphe layer. There is no need to filter nodes for the hyphe layer.

![The hyphe layer in Gephi](./doc/gephi_hyphe_layer.png)

#### the overton layer

By using Filter / library / attributes / partition / layer (edge), keep only the edges that are in the overton layer by selecting both "overton" and "hyphe_overton" partitions.

![The overton layer with hyphe nodes in Gephi](./doc/gephi_overton_layer_with_hyphe_nodes.png)

By keeping the hyphe nodes, you can appreciate the situation of the overton layer into the Hyphe context.

But you can also remove the hyphe nodes to explore the overton layer.
To do so add as a subfilter, Filter / library / attributes / partition / layer (node) and keep only the "hyphe_overton" partition.

![the overton layer](./doc/gephi_overton_layer.png)

Once isolated the overton layer can be spatialized and sized correclty.
The spactialization is as usual. For the node size, one needs to first run the degree statistics modules before applying inDegree to node size to update the degree metrics after filtering (this is not done automatically).

![the overton layer edited](./doc/gephi_overton_layer_spatialized_sized.png)

The overton layer containing only a small subset of web entities (~13%), one approach would be to compate it to the hyphe layer containing only the overton nodes. This wen be done by filtering the nodes by their layer attribute in the hyphe layer. By choosing a stable set of nodes, the structure effects of both layers will be easier to discuss.

Note that the edge weight are by default ignored. If you want to add the number of document citations as edge weight :

- go to data laboratory
- click on edges tab
- "copy data to on other column"
- select "nb_doc_citation"
- select "Weight"

But Gephi does not handle large edge weight correctly. The geometry size is adjusted linearly with the weight value which makes the viz unreadable very quickly. And there is no way that I know off to apply a log or square root modifier on edge size.
